export type Value = {
	U16?: number;
	I16?: number;
	I32?: number;
	Float?: number;
};

export enum ValueType {
	U16 = "U16",
	I16 = "I16",
	I32 = "I32",
	Float = "Float",
}

export const Zero: Value = { U16: 0 }

export function ToType(value: Value): ValueType {
	if (value.Float != undefined) {
		return ValueType.Float;
	} else if (value.I16 != undefined) {
		return ValueType.I16;
	} else if (value.U16 != undefined) {
		return ValueType.U16;
	} else if (value.I32 != undefined) {
		return ValueType.I32;
	} else {
		console.warn("Unable to detect type for Value", value);
		return ValueType.Float;
	}
}

export function GetValue(value: Value): number {
	if (value.Float != undefined) {
		return value.Float;
	} else if (value.I16 != undefined) {
		return value.I16;
	} else if (value.U16 != undefined) {
		return value.U16;
	} else if (value.I32 != undefined) {
		return value.I32;
	} else {
		console.warn("Unable to detect type for Value", value);
		return 0.0;
	}
}

export function Cast(value: Value, ty: ValueType): Value {
	return {
		[ty]: GetValue(value),
	}
}