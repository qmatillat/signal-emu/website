export const GetVal = (e: Event) =>
	(e.target as HTMLInputElement).value;